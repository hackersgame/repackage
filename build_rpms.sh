#!/bin/bash
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 
   exit 1
fi

stuff_to_build=`dirname "$0"`/SPECnDATA

#build rpm dirs
rpm_root=`dirname "$0"`/rpm_root

echo "Setting-up $rpm_root to build RPMs"
mkdir $rpm_root 2>/dev/null
mkdir $rpm_root/BUILD 2>/dev/null
mkdir $rpm_root/RPMS 2>/dev/null
mkdir $rpm_root/SOURCES 2>/dev/null
mkdir $rpm_root/SPECS 2>/dev/null
mkdir $rpm_root/SRPMS 2>/dev/null

echo "Copying data..."
#copy spec files
cp $stuff_to_build/*.spec $rpm_root/SPECS/

#copy data files
cp $stuff_to_build/*.tar.gz $rpm_root/SOURCES/



SPECs=$rpm_root/SPECS/*
for spec in $SPECs 
do
    echo $spec
    rpmbuild -ba --buildroot $rpm_root $spec
    exit 1
done
