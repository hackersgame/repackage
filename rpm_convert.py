#!/usr/bin/python3
import os
import time
import shutil
import stat
import pathlib
import tarfile

script_path = os.path.dirname(os.path.realpath(__file__))
changeLog = []
with open(script_path + "/deb_dump/files_installed.txt") as fh:
    changeLog = fh.readlines()

if len(changeLog) < 1:
    print("ChangeLog empty, Something went wrong...")
    exit(1)
print("converting...")
#clean up old build files if needed
try:
    shutil.rmtree(script_path + "/rpm_build")
except FileNotFoundError:
    pass
#setup new build folder
os.mkdir(script_path + "/rpm_build")

spec_n_data = script_path + "/SPECnDATA/"
try:
    os.mkdir(spec_n_data)
except FileExistsError:
    pass


#takes a path to a file to include int he RPM
#outputs rpm.spec crap
def RPM_file(file_to_install):
    print("Installing file: " + file_to_install)
    install_where = file_to_install.split("/chroot")[-1]
    install_what = file_to_install.split("/")[-1]
    needed_dir = install_where[:len(install_what) * -1]
    file_permissions = oct(os.stat(file_to_install)[stat.ST_MODE])[-3:]
    #print(str(file_permissions))
    #print("needed dir: " + needed_dir)
    #print("file name: " + install_what)
    #print("installed should be: " + install_where)
    install_line_dir_part = "mkdir -p %{buildroot}" + needed_dir
    install_line = f"install -m {str(file_permissions)} {install_what} $RPM_BUILD_ROOT{install_where}"
    return(install_line_dir_part + "\n" + install_line, install_where)

RPM_install = []
RPM_files = []
LINKs = {}
for change in changeLog:
    if "chroot/var/lib/dpkg" in change:
        continue
    if "Only in" in change:
        #the change will look something like: 
        #Only in ./chroot/usr/share: python-wheels
        file_part = change.split("Only in")[-1]               # ./chroot/usr/share: python-wheels
        file_part = file_part.strip()[1:]                     #./chroot/usr/share: python-wheels
        file_part = file_part.split(":")[0] + '/'             #/chroot/usr/share/
        file_part = file_part + change.split(":")[-1].strip() #/chroot/usr/share/python-wheels
        new_file = script_path + file_part
        
        try:
            #files might be named the same... This will overwrite crap it should not TODO
            shutil.copyfile(new_file, script_path + "/rpm_build/" + new_file.split('/')[-1])
        #handle directory of files
        except IsADirectoryError:
            new_dir = new_file
            if os.path.getsize(new_dir) == 0:
                print("Looks like a empty folder: " + new_file)
                RPM_install.append("mkdir -p %{buildroot}" + new_file)
                continue
            else:
                for filename in pathlib.Path(new_dir).rglob('*'):
                    filename = str(filename)
                    if os.path.isfile(filename):
                        shutil.copyfile(filename, script_path + "/rpm_build/" + filename.split('/')[-1])
                        new_install_line, new_file_line = RPM_file(filename)
                        RPM_install.append(new_install_line)
                        RPM_files.append(new_file_line)
                        continue
                    else:
                        print("Looks like a empty folder: " + filename)
                        RPM_install.append("mkdir -p %{buildroot}" + filename)
                        continue
        #handle link
        except FileNotFoundError:
            new_link_dst = new_file
            #we have a broken link file
            broken_path = os.readlink(new_link_dst)
            #handle multi level links
            while True:
                full_path = script_path + "/chroot" +  broken_path
                if os.path.isfile(full_path):
                    break
                #we have a link to a link
                print("Link: " + full_path + " is a link..")
                broken_path = os.readlink(full_path)
                print("To: " + broken_path)
            install_path = new_link_dst.split("/chroot")[-1]
            print("file: " + full_path)
            print("Linked: " + install_path)
            print("Bro... link files?")
            LINKs[full_path] = install_path
            #copy file
            shutil.copyfile(full_path, script_path + "/rpm_build/" + full_path.split('/')[-1])
            #add file
            new_install_line, new_file_line = RPM_file(full_path)
            RPM_install.append(new_install_line)
            RPM_files.append(new_file_line)
            continue

        #print("Copying_file: " + new_file)
        #not sure how new_file can still be a directory, but it can
        if os.path.isfile(new_file):
            new_install_line, new_file_line = RPM_file(new_file)
            RPM_install.append(new_install_line)
            RPM_files.append(new_file_line)
        else:
            print("Looks like a empty directory: " + new_file)
            dir_name = new_file.split('/chroot')[-1]
            RPM_install.insert(0,"mkdir -p %{buildroot}" + dir_name)
            RPM_files.insert(0,dir_name)

def LINKs_to_str(LINKs):
    #ln -s $RPM_BUILD_ROOT/usr/share/janit/janit.py $RPM_BUILD_ROOT/usr/bin/janit || true
    return_str = ""
    for link in LINKs:
        return_str = return_str + f"ln -s $RPM_BUILD_ROOT{link} $RPM_BUILD_ROOT{LINKs[link]}\n" 
    return return_str


def read_deb_control():
    deb_control_file = script_path + "/deb_dump/deb/control"
    #sometimes this is compressed as control.tar.gz
    if not os.path.isfile(deb_control_file):
        control_file_tar_gz = script_path + "/deb_dump/deb/control.tar.gz"
        extract_to = script_path + "/deb_dump/deb/"
        with tarfile.open(control_file_tar_gz) as tar:
            tar.extractall(extract_to)

    return_data = {}
    for line in open(deb_control_file).readlines():
        if ":" in line:
            line = line.split(":")
            return_data[line[0]] =line[1].strip()
    return return_data
    """Package: binutils-common
    Source: binutils
    Version: 2.31.1-16
    Architecture: amd64
    Maintainer: Matthias Klose <doko@debian.org>
    Installed-Size: 13000
    Breaks: binutils (<< 2.29.1-3.1~)
    Replaces: binutils (<< 2.29.1-3.1~)
    Section: devel
    Priority: optional
    Multi-Arch: same
    Homepage: https://www.gnu.org/software/binutils/
    Description:
    """
spec_info = read_deb_control()
if "Package" in spec_info:
    name = spec_info["Package"]
if "Description" in spec_info:
    summary = spec_info["Description"]

version = 1
if "Version" in spec_info:
    version = spec_info["Version"]
  
if "Architecture" in spec_info:
    arch = spec_info["Architecture"]
    if arch == "all":
        arch = "noarch"
    if arch == "amd64":
        arch = "x86_64"
license = "TODO" #TODO
new_line = "\n"

spec_file = f"""
Name:           {name}
Version:        1
Release:        1
Summary:        {summary}


BuildArch:      {arch}
License:        {license}
Source0:        {name}.tar.gz

%description
{summary}

%install
{new_line.join(RPM_install)}

%post
{LINKs_to_str(LINKs)}



%files
{new_line.join(RPM_files)}
"""
print("\n\nSpec:\n" + spec_file)
#write spec
with open(spec_n_data + name + ".spec", "w+") as fh:
    fh.write(spec_file)

#write tar.gz data
with tarfile.open(spec_n_data + name + ".tar.gz", "w:gz") as tar:
    #tar.add(script_path + "/rpm_build", arcname=os.path.basename(script_path + "/rpm_build"))
    tar.add(script_path + "/rpm_build", arcname=os.path.basename(script_path + "/rpm_build/"))


