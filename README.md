Repackage
=========

Setup:
------
* cd to main folder
* run ./setup_new_chroot.sh

Running:
--------
* cd to main folder
* ./repackage <some name that can be installed via apt>

license
-------
GPL3
By David Hamner
