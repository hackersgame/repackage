#!/bin/bash
CHROOT=`dirname "$0"`/chroot
CHROOT_BK=`dirname "$0"`/chroot_b4
BUILD_DATA_FOLDER=`dirname "$0"`/deb_dump
PATH=$PATH:`dirname "$0"`

echo "Installing $1 into chroot"
##############################################################################
#######################Backup chroot before install###########################
##############################################################################
rm -r $CHROOT_BK 2>/dev/null
echo "running backup..."
cp -arP $CHROOT $CHROOT_BK


##############################################################################
###############################Setup chroot###################################
##############################################################################
echo "Setting up chroot..."
cmount

##############################################################################
##############################Run install CMD#################################
##############################################################################
#chroot in and exit
echo "Running install"
chroot $CHROOT /bin/bash -c "dpkg -i --force-all $1"

##############################################################################
###############################Unmount stuff##################################
##############################################################################
echo "cleaning up..."
cumount

##############################################################################
##############################Output changes##################################
##############################################################################
#checking for changes
echo "checking for changes..."
rm -r $BUILD_DATA_FOLDER 2> /dev/null
mkdir $BUILD_DATA_FOLDER
diff -qr --no-dereference $CHROOT $CHROOT_BK | grep -v /dev | tee $BUILD_DATA_FOLDER/files_installed.txt
echo "extracting deb..."
mkdir  $BUILD_DATA_FOLDER/deb
cp $CHROOT/$1 $BUILD_DATA_FOLDER/deb
cd $BUILD_DATA_FOLDER/deb
ar x ./*.deb
tar xvf ./control.tar.xz
cd -

##############################################################################
##############################Rebuild as rpm##################################
##############################################################################
rpm_convert.py
