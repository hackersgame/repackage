#!/bin/bash
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 
   exit 1
fi
CHROOT=`dirname "$0"`/chroot
PATH=$PATH:`dirname "$0"`
echo $CHROOT
#make dir if needed
[ ! -d "$CHROOT" ] && mkdir $CHROOT
#exit if chroot is not empty
if [ "$(ls -A $CHROOT)" ]
then
    echo $CHROOT not empty
    exit 1
fi
#setup chroot
#Ubuntu 18.04
debootstrap --variant=buildd bionic $CHROOT http://archive.ubuntu.com/ubuntu/
#debian 10
#debootstrap buster $CHROOT http://deb.debian.org/debian/
cp /etc/hosts $CHROOT/etc/hosts
cp /etc/resolv.conf $CHROOT/etc/resolv.conf

echo -e "Testing chroot...\n"
#First chroot makes a few files:
#setup chroot
cmount
#chroot in and exit
chroot $CHROOT /bin/bash -c "cat /etc/*rel*"
echo -e "\ncleaning up..."
#unmount chroot
cumount
echo "done"
